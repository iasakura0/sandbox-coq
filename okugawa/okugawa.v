Section Reachability.
  Require Import List.
  Import ListNotations.
  Require Import LibTactics.

  Variable has_meth : nat -> bool.
  Variable parent : nat -> nat.
  
  Fixpoint path (src : nat) (cs : list nat) (dst : nat) :=
    match cs with
      | [] => src = dst
      | c :: cs => parent src = c /\ path c cs dst
    end.

  Require Import NPeano.

  Definition obj := 0.
  Definition cls_dec := Nat.eq_dec.
  Require Import Recdef.

  Definition parent_ok classes : Prop := forall c, List.In c classes -> List.In (parent c) classes.
  Definition cs_ok classes :=
    ~ exists (c : nat) (cs : list nat),
        List.In c cs /\
        incl cs classes /\
        path c cs c.
  
  Lemma remove_incl c classes : incl (remove cls_dec c classes) classes.
  Proof.
    induction classes; unfold incl; simpl; eauto.
    unfold incl in *; intros c' Hc'; destruct cls_dec; eauto.
    simpl in *; firstorder.
  Qed.
  
  Lemma NoDup_remove c classes : NoDup classes -> NoDup (remove cls_dec c classes).
  Proof.
    induction 1; simpl; [constructor | ].
    destruct cls_dec; eauto.
    constructor; simpl; eauto.
    intros Hc; apply H.
    apply remove_incl in Hc; eauto.
  Qed.

  Function dispatch_iter (classes : list nat) (c : nat) {measure length classes} :=
    if has_meth c then Some c
    else if cls_dec c obj then None
    else if in_dec cls_dec c classes then dispatch_iter (remove cls_dec c classes) (parent c)
    else None.
  Proof.
    intros classes c Hmeth Hneq.
  
  

  Definition dispatch := dispatch_iter (List.length classes).

  Lemma dispatch_ok_immd (c : nat) :
    List.In c classes ->
    has_meth c = true -> dispatch c = Some c.
  Proof.
    unfold dispatch; destruct classes; simpl; [destruct 1|].
    intros _ H; rewrite H; eauto.
  Qed.

  Lemma dispatch_ok (c : nat) :
    List.In c classes -> has_meth c = false ->
    (exists p cs, incl cs classes /\ List.In p classes /\ has_meth p = true) ->
    exists p', dispatch c = Some p'.
  Proof.
    unfold dispatch; remember (length classes) as n eqn:Hlen.
    generalize dependent classes.
    clear classes_NoDup cs_acyclic parent_valid classes.
    induction n; intros ? ? ? ? ? HIn Hcmeth (p & cs & Hp & Hcsin & Hpmt).
    - destruct classes; simpl in *; try tauto; congruence.
    - lets (cs1 & cs2 & ?): (>> in_split HIn); subst.
      lets Hp': (>> IHn (cs1 ++ cs2)); exploit Hp'.
      + eapply NoDup_remove_1; eauto.
      + intros (c0 & cs0 & Hc0 & Hcs0 & Hp0); apply cs_acyclic.
        exists c0 cs0; repeat split; eauto.        
        unfold incl in *; intros a Ha; specialize (Hcs0 a Ha).
        rewrite in_app_iff in *; simpl; tauto.
      + intros c0 H.
        assert (H' : In c0 (cs1 ++ c :: cs2)) by (rewrite in_app_iff in *; simpl in *; tauto).
        lets Hin: (>> parent_valid H').
        
  Theorem dispatch_correct (c : nat) :
    List.In c classes ->
    (* if c has the method, dispatch c returns c *)
    (has_meth c = true -> dispatch c = Some c) /\
    (* if c does not have the method, but if there exists parent that has the method,
       dispatch c returns the parent *)
    (has_meth c = false /\
     forall p, 
     (exists cs,
        incl cs classes /\ List.In p classes /\
        path c cs p /\ List.Forall (fun c => has_meth c = false) cs /\
        has_meth p = true) -> dispatch c = Some p) /\
    ((forall p cs, incl cs classes -> List.In p classes) -> dispatch c = None).