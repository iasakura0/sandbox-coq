Require Import List Permutation Sorted Orders Arith.
Require Import Recdef Omega.

Module Sort(Import X:Orders.TotalLeBool).
  Import ListNotations.
  
  Fixpoint partition (p : t) (xs : list t) : (list t * list t) :=
    match xs with
      | [] => ([], [])
      | x :: xs =>
        let (ltps, geps) := partition p xs in
        if leb x p then (x :: ltps, geps)
        else (ltps, x :: geps)
    end.
 
  Definition length_order (l1 l2 : list t) := length l1 < length l2.

  Theorem lengthOrder_wf : well_founded length_order.
  Proof.
    apply (well_founded_lt_compat _ (@length t)).
    unfold length_order; intros ? ? H; auto.
  Defined.
  
  Lemma partition_ordered1 (x p : t) (xs : list t) : length_order (fst (partition p xs)) (x :: xs).
  Proof.
    unfold length_order; induction xs; simpl; try omega.
    destruct (partition p xs), (leb a p); simpl in *; omega.
  Qed.

  Lemma partition_ordered2 (x p : t) (xs : list t) : length_order (snd (partition p xs)) (x :: xs).
  Proof.
    unfold length_order; induction xs; simpl; try omega.
    destruct (partition p xs), (leb a p); simpl in *; omega.
  Qed.
 
  Hint Resolve partition_ordered1 partition_ordered2.

  Definition sort_body (xs : list t)
             (rec : forall ys, length_order ys xs -> list t -> list t) (acc : list t) : list t.
  Proof.
    refine (
    match xs as xs0 return xs = xs0 -> list t with
      | [] => fun _ => acc
      | [x] => fun _ => x :: acc
      | x :: y :: xs => fun Heq =>
          let xys := partition x (y :: xs) in
          rec (fst xys) _ (x :: (rec (snd xys) _ acc))
    end eq_refl); abstract (unfold xys; subst; auto).
  Defined.

  Definition qsort0 (xs : list t) : list t -> list t :=
    Fix lengthOrder_wf (fun _ => list t -> list t) sort_body xs.

  Definition qsort xs := qsort0 xs [].

  Section Partition_prop.
    Lemma partition_correct1 (p : t) (xs : list t) :
      forall x, In x (fst (partition p xs)) -> leb x p = true.
    Proof.
      induction xs; simpl; try tauto.
      case_eq (partition p xs); intros l ? H x.
      case_eq (leb a p); simpl; intros Heq H'.
      - rewrite H in *; simpl in *; destruct H'; subst; firstorder.
      - rewrite H in *; simpl in *; firstorder.
    Qed.

    Lemma partition_correct2 (p : t) (xs : list t) :
      forall x, In x (snd (partition p xs)) -> leb x p = false.
    Proof.
      induction xs; simpl; try tauto.
      case_eq (partition p xs); intros l ? H x.
      case_eq (leb a p); simpl; intros Heq H'.
      - rewrite H in *; simpl in *; firstorder.
      - rewrite H in *; simpl in *; destruct H'; subst; firstorder.
    Qed.

    Lemma partition_permute (p : t) (xs : list t) :
      let (ls, gs) := partition p xs in
      Permutation (p :: xs) (ls ++ p :: gs).
    Proof.
      induction xs; simpl.
      - repeat constructor.
      - destruct (partition p xs) as [ls gs] eqn:Hlgs.
        destruct (leb a p); simpl.
        + eapply perm_trans; [apply perm_swap|].
          eapply perm_trans; [apply perm_skip, Permutation_refl|].
          apply perm_skip; auto.
        + eapply perm_trans; [apply perm_swap|].
          eapply perm_trans; [apply perm_skip; apply IHxs|].
          eapply perm_trans; [apply Permutation_middle|].
          apply Permutation_app_head.
          apply perm_swap.
    Qed.

    Lemma partition_length : forall p xs, 
      length (fst (partition p xs)) + length (snd (partition p xs)) = length xs.
    Proof.
      induction xs; simpl; auto.
      destruct (partition p xs), (leb a p); simpl in *; omega.
    Qed.
  End Partition_prop.

  Require Import FunctionalExtensionality.
  Require Import ProofIrrelevance.

  Lemma Fix_eq_ok : 
    forall (x : list t)
     (f g : forall y : list t, length_order y x -> list t -> list t),
   (forall (y : list t) (p : length_order y x), f y p = g y p) ->
   sort_body x f = sort_body x g.
  Proof.
    unfold sort_body; intros; extensionality acc.
    repeat (match goal with
              | [ |- context[match ?E with | [] => _ | _ :: _ => _ end] ] => destruct E
            end); repeat (simpl; try rewrite H0; auto).
    repeat (f_equal; try apply proof_irrelevance).
  Qed.

  Hint Resolve Fix_eq_ok.
  Hint Unfold qsort qsort0.
  Ltac induct_wf xs :=
    let l := fresh "l" in
    let H := fresh in
    remember (length xs) as l eqn:H; revert xs H; induction l using lt_wf_ind;
    intros xs.

  Ltac unfold_body :=
    repeat (cutrewrite (Fix lengthOrder_wf (fun _ => list t -> list t) sort_body = qsort0); [|auto]).
  
  Lemma qsort_acc_concat : forall  (xs acc : list t), qsort0 xs acc = qsort xs ++ acc.
  Proof.
    intros xs; remember (length xs) as lxs; revert xs Heqlxs.
    induction lxs using lt_wf_ind; intros xs Heq.
    destruct xs as [ |x [| y xs]]; repeat autounfold in *; intros acc.
    - rewrite Fix_eq; simpl; auto.
    - rewrite Fix_eq; simpl; auto.
    - rewrite Fix_eq; auto; simpl; unfold_body.
      destruct (leb y x), (partition x xs) eqn:Heq'; simpl in *.
      + pose proof (partition_length x xs) as Hps; rewrite Heq' in Hps; simpl in *.
        rewrite (H (length (y :: l))); simpl; try omega; unfold_body.
        assert (Hlt : length l0 < lxs) by omega.
        rewrite (H (length l0) Hlt l0); auto; simpl; try omega.
        unfold_body.
        cutrewrite (x :: qsort0 l0 [] ++ acc = [x] ++ qsort0 l0 [] ++ acc); auto.
        rewrite !app_assoc, <-(app_assoc _ [x] _); erewrite <-H; auto.
        simpl; omega.
      + pose proof (partition_length x xs) as Hps; rewrite Heq' in Hps; simpl in *.
        rewrite (H (length l)); simpl; try omega; unfold_body.
        assert (Hlt : length (y :: l0) < lxs) by (simpl; omega).
        rewrite (H (length (y :: l0)) Hlt (y::l0)); auto; simpl; unfold_body.
        cutrewrite (x :: qsort0 (y :: l0) [] ++ acc = [x] ++ qsort0 (y :: l0) [] ++ acc); auto.
        rewrite !app_assoc, <-(app_assoc _ [x] _); erewrite <-H; auto.
        simpl; omega.
  Qed. 

  Lemma qsort_perm : forall l, Permutation l (qsort l).
  Proof.
    intros l; induct_wf l.
    destruct l as [|x [|y xs]]; simpl; intros Heq; unfold qsort, qsort0;
      simpl; rewrite Fix_eq; auto.
    - destruct (partition x (y :: xs)) as [lxs gxs] eqn:Hxs.
      simpl.
      let t := eval simpl in  (partition x (y :: xs)) in cutrewrite (t = partition x (y :: xs)); [|auto].
      rewrite Hxs, !qsort_acc_concat, app_nil_r; simpl.
      eapply perm_trans.
      pose proof (partition_permute x (y :: xs)) as Hpp; rewrite Hxs in Hpp; apply Hpp.
      pose proof (partition_length x (y :: xs)) as Hlen; rewrite Hxs in Hlen; simpl in Hlen.
      apply Permutation_add_inside; eapply H; auto; subst; omega.
  Qed.      

  Notation Sorted := (LocallySorted (fun x y => leb x y = true)).

  Lemma app_sorted (xs ys : list t) (p : t) :
    Sorted xs -> Sorted ys ->
    (forall t, In t xs -> leb t p = true) ->
    (forall t, In t ys -> leb t p = false) ->
    Sorted (xs ++ p :: ys).
  Proof.
    induction xs; intros Hxs Hys Hxs' Hys'; simpl.
    - destruct ys eqn:Heqys; try constructor; auto. 
      destruct (leb p t0) eqn:Heq; auto.
      assert (leb t0 p = false) by (apply Hys'; simpl; auto).
      pose proof(leb_total t0 p) as [|]; congruence.
    - destruct xs; simpl.
      + repeat constructor.
        apply IHxs; repeat constructor; auto.
        apply Hxs'; simpl; auto.
      + assert (Sorted (t0 :: xs ++ p :: ys)).
        { apply IHxs; auto.
          inversion Hxs; subst; try constructor; auto.
          intros; apply Hxs'; simpl; auto. }
        apply LSorted_consn; auto.
        inversion Hxs; subst; auto.
  Qed.
      
  Lemma Sorted_sort : forall l, Sorted (qsort l).
  Proof.
    intros l; remember (length l) as ll; revert l Heqll; induction ll using lt_wf_ind; intros l Heq.
    unfold qsort. destruct l as [| x [| y xs]]; simpl;
    unfold qsort0; rewrite Fix_eq; auto.
    - simpl; constructor.
    - simpl; constructor.
    - destruct (partition x (y :: xs)) as [lxs gxs] eqn:Hxs.
      simpl.
      let t := eval simpl in  (partition x (y :: xs)) in cutrewrite (t = partition x (y :: xs)); [|auto]; rewrite Hxs; simpl.
      unfold_body.
      rewrite !qsort_acc_concat, app_nil_r; simpl.
      pose proof (partition_length x (y :: xs)) as Hlen; rewrite Hxs in Hlen; simpl in Hlen.
      apply app_sorted; [apply (H (length lxs))| apply (H (length gxs)) |..]; subst; simpl; try omega.
      intros t Ht.
      apply (Permutation_in _ (Permutation_sym (qsort_perm lxs))) in Ht.
      apply (partition_correct1 x (y :: xs)); rewrite Hxs; auto.
      intros t Ht.
      apply (Permutation_in _ (Permutation_sym (qsort_perm gxs))) in Ht.
      apply (partition_correct2 x (y :: xs)); rewrite Hxs; auto.
  Qed.
End Sort.

Module NatOrder <: TotalLeBool.
  Definition t := nat.
  Definition leb x y := leb x y.
  Theorem leb_total : forall a1 a2, leb a1 a2 = true \/ leb a2 a1 = true.
  Proof.
    intros a1 a2; destruct (leb a1 a2) eqn:H12, (leb a2 a1) eqn:H21; try tauto.
    apply leb_iff_conv in H12; apply leb_iff_conv in H21; omega.
  Qed.
End NatOrder.

Module IntSort := Sort (NatOrder).
Import ListNotations.
Eval compute in IntSort.qsort [20; 50; 10; 20; 30].