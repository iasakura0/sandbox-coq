(* TPPmark 2010 *)

Require Import ssreflect ssrfun ssrbool eqtype ssrnat choice seq.
Require Import fintype finfun bigop ssralg ssrnum poly ssrint.

Set Implicit Arguments.
Unset Strict Implicit.

Open Scope R.

Definition board := (seq (int * bool)).

Definition strategy := forall b : board, {x : int | ~~(x \in map (fun x => x.1) b)}.

Inductive game (f g : strategy) : bool -> board -> Prop :=
| Game0 : game f g true [:: ]
| GameA : forall (b : board),
            game f g true b -> 
            game f g false [:: ((proj1_sig (f b)), true) & b]
| GameD : forall (b : board),
            game f g false b -> 
            game f g true [:: ((proj1_sig (g b)), false) & b].

Definition st_impl0 := fun (b : board) =>  (foldr (fun x y => Num.max x.1 y) 0%R b + 1).

Lemma gt_st : forall (b : board) x, (x \in b) -> x.1 < st_impl0 b.
Proof.
  rewrite /st_impl0; elim=>[|a b IHb] x H // /=.
  rewrite in_cons in H; move:H=>/orP [/eqP H|H]; subst; rewrite ltz_addr1.
  - rewrite Num.Theory.ler_maxr Num.Theory.lerr=>//.
  - apply IHb in H=>//.
    rewrite Num.Theory.ler_maxr; rewrite ltz_addr1 in H; rewrite H orbT=>//.
Qed.

Lemma ineq_st : forall (b : board) x, (x \in b) -> x.1 != st_impl0 b.
Proof.
  move=>b x H; move: (gt_st H)=>H'.
  rewrite Num.Theory.ltr_def in H'; move :H'=>/andP [/eqP]; intros; apply/eqP; congruence.
Qed.

Lemma st0_ok : forall b, st_impl0 b \notin [seq x.1 | x <- b].
  move=>b; apply/memPn=>x Hx; apply/negP=>/eqP H; rewrite H in Hx.
  move: Hx=>/mapP=>Hx; case: Hx=>x' Hx' Hc.
  apply ineq_st in Hx'; move:Hx'=>/eqP=>Hx'; rewrite Hc in Hx'.
  apply Hx'=>//.
Qed.

Definition strategy0 : strategy.  
  rewrite /strategy; refine (fun b => exist _ (st_impl0 b) _).
  apply st0_ok.
Defined.

Definition near (n1 n2 : int) : bool := ((n1 == n2 + 1) || (n2 + 1 == n1)).

Definition default : (int * bool) := (0, true)%Z.

Definition free (p : (int * bool)) (b : board) := ~~ has (fun x => (p.1 + 1 == x.1)) b.

Definition find_free (b : board) : option int :=
  let: i := find (fun p => p.2 && free p b) b in
  if i == size b then None else Some (nth default b i).1.

Lemma find_free_ok (b : board) (i : int) :
  find_free b = Some i -> ~~(i + 1 \in map (fun x => x.1) b).
Proof.
  rewrite /find_free; move H: (find _ _ == size _)=>[]; first by congruence.
  inversion 1; subst.
  apply/negP=>/mapP [x H' H''].
  move: (find_size (fun p => p.2 && free p b) b).
  rewrite (leq_eqVlt (find (fun p => p.2 && free p b) b) (size b))=>/orP [|]; first by congruence.
  rewrite -has_find; move/(nth_find _)=>/(_ default) /andP [_].
  rewrite /free=> /hasPn /(_ x H'); rewrite H''=>/eqP //.
Qed.  

Eval compute in find_free [:: (1, true); (1+1, false); (1+1+1, true)].

Definition consec (p : (int * bool)) (b : board) :=
  has (fun x => (x.1 == p.1 + 1) && x.2) b && ~~has (fun x => x.1 == p.1 - 1) b.

Definition find_consec (b : board) : option int :=
  let i := find (fun p => p.2 && consec p b) b in
  if i == size b then None else Some (nth default b i).1.

Lemma find_consec_ok (b : board) (i : int) :
  find_consec b = Some i -> ~~(i - 1 \in map (fun x => x.1) b).
Proof.
  rewrite /find_consec; move H: (find _ b == size b)=>[]; first by congruence.
  inversion 1; subst.
  apply/negP; move/mapP=>[x H' H''].
  have: (find (fun p => p.2 && consec p b) b < size b)%nat.
  { move: (find_size (fun p => p.2 && consec p b) b).
    rewrite leq_eqVlt=> /orP [|]; [rewrite H|]=>//. }
   rewrite -has_find; move/(nth_find _)=>/(_ default) /andP [_ ].
  rewrite /consec=>/andP [_] /hasPn /(_ x H'); rewrite H''=> /eqP=>//.
Qed.  

Eval compute in find_consec [:: (1, true); (1+1, true); (1+1+1, false)].

Definition st1_impl (b : board) :=
  match find_consec b with
    | Some n => n - 1
    | None =>
      match find_free b with
        | Some n => n + 1
        | None => proj1_sig (strategy0 b)
      end
  end.

Hint Resolve st0_ok find_free_ok find_consec_ok.

Lemma st1_impl_ok (b : board) : ~~(st1_impl b \in map (fun x => x.1) b).
Proof.
  rewrite /st1_impl; case_eq (find_consec b)=>[i H|H]; [|case_eq (find_free b)=>[i H'|H']];
    try rewrite H H'; simpl; auto.
Qed.

Definition strategy1 : strategy.
  rewrite /strategy; refine (fun b => exist _ (st1_impl b) _); apply st1_impl_ok.
Defined.

Definition invariant1 (b : board) :=
  (forall i, (i, true) \in b ->
     if (i+1, true) \in b then (i-1, false) \in b /\ (i+2, false) \in b
     else (i+1, false) \in b).

Hint Extern 1 => match goal with [H : is_true (?X != ?X) |- _] => move: H=>/eqP // end.

Lemma game_func (b : board) (f g : strategy) (t : bool):
  game f g t b -> forall x y, x \in b -> y \in b -> x.1 == y.1 -> x.2 == y.2.
Proof.
  elim=>// {b t} b game0 IH x y.
  - move :(f b)=>[fb Hfb] /=. rewrite !in_cons=>/orP [/eqP|] Hx /orP [/eqP|] Hy /eqP Heqxy;
    apply /eqP; try congruence;
    match goal with [H : is_true (?x \in b) |- _] => 
      (have H': (x.1 \in [seq t.1 | t <- b]) by (apply/mapP; exists x=>//))
    end.
    + move: Hfb=>/memPn /(_ y.1 H') /eqP; subst; simpl in *; congruence.
    + move: Hfb=>/memPn /(_ x.1 H') /eqP; subst; simpl in *; congruence.
    + move: IH=>/(_ x y Hx Hy)=>IH'; apply/eqP; apply IH'; apply/eqP=>//.
  - move :(g b)=>[fb Hfb] /=; rewrite !in_cons=>/orP [/eqP|] Hx /orP [/eqP|] Hy /eqP Heqxy;
    apply /eqP; try congruence;
    match goal with [H : is_true (?x \in b) |- _] => 
      (have H': (x.1 \in [seq t.1 | t <- b]) by (apply/mapP; exists x=>//))
    end.
    + move: Hfb=>/memPn /(_ y.1 H') /eqP; subst; simpl in *; congruence.
    + move: Hfb=>/memPn /(_ x.1 H') /eqP; subst; simpl in *; congruence.
    + move: IH=>/(_ x y Hx Hy)=>IH'; apply/eqP; apply IH'; apply/eqP=>//.
Qed.

Import GRing.

Lemma strategy_prop1 (b : board) (f : strategy) :
  invariant1 b -> (sval (f b) + 1, true) \in b ->
  sval (f b) - 1 \notin [seq t.1 | t <- b] ->
  (st1_impl ((sval (f b), true) :: b)) == sval (f b) - 1.
Proof.
  move: (f b)=>[x Hnin]=>/=.
  rewrite /invariant1 /strategy1 /st1_impl => Hinv Hin Hx'bn =>/=.
  suff: (find_consec ((x, true) :: b) == Some x) by (move/eqP => ->).
  rewrite /find_consec.
  have H: (consec (x, true) ((x, true) :: b)).
  { rewrite/consec; apply/andP; split.
    - apply/hasP; exists (x+1, true).
      + rewrite in_cons; apply/orP; right; auto.
      + simpl; rewrite eqxx=>//.
    - apply/negP=>/hasP [y Hy0 Hy1]; simpl in *.
      have H : (y \in b).
      { rewrite in_cons in Hy0; move: Hy0=>/orP [/eqP|]=>//.
        move=>Heq; rewrite Heq in Hy1; simpl in *.
        rewrite -subr_eq0 opprD addrA addrN opprK add0r in Hy1=>//. }
      move: Hx'bn=>/memPn.
      have Hy: (y.1 \in [seq t.1 | t <- b]) by (apply/mapP; exists y).
      move /(_ y.1 Hy) /negP=>//. }
  have H': (find (fun p => p.2 && consec p ((x, true) :: b)) ((x, true) :: b) <
           size ((x, true) :: b))%N.
  { rewrite <-has_find; apply/hasP; exists (x, true); simpl.
    - rewrite in_cons; apply/orP; left; apply/eqP; auto.
    - apply: H. }
  rewrite ltn_eqF=>//.
  simpl; rewrite H; simpl; rewrite eqxx=>//.
Qed.

Lemma r_neq_sr (n : int) : n != n + 1.
Proof.
  apply/negP.
  rewrite -subr_eq0 opprD addrA addrN add0r; inversion 1.
Qed.

Lemma strategy_prop2 (b : board) (f : strategy) :
  invariant1 b -> 
  sval (f b) + 1 \notin [seq t.1 | t <- b] ->
  (st1_impl ((sval (f b), true) :: b)) == sval (f b) + 1.
Proof.
  rewrite /st1_impl /invariant1; 
  move: (f b)=>[fb Hfb] Hinv0 Hnin1; rewrite /st1_impl; simpl in *.
  have: (find_consec ((fb, true) :: b) = None).
  { rewrite /find_consec.
    match goal with | [|- context[if ?b then _ else _]] =>
      suff: (is_true b) by move=>->
    end.
    suff: (~~has (fun p => p.2 && consec p ((fb, true) :: b)) ((fb, true) :: b)).
    { rewrite has_find=>/negP H.
      move: (find_size (fun p => p.2 && consec p ((fb, true) :: b))
                       ((fb, true) :: b));
      rewrite leq_eqVlt=>/orP []; auto. }
    rewrite/consec; apply/negP=>/hasP [x Hc0 /and3P [Hc1 Hc2 Hc3]].
    move: Hc0; rewrite in_cons=>/orP [/eqP|] H; subst.
    - move: Hc2=> /hasP [t Ht1 /andP [Ht2 Ht3]].
      move: Ht2 Ht3 Ht1; case t => /= [t1 t2] /eqP Ht2 Ht3; rewrite Ht2 Ht3 in_cons.
      have Hc: (~~((fb+1, true) \in b)).
      { apply/negP => H; move: Hnin1=>/mapP; apply; exists (fb+1, true); auto. }
      move/orP=>[]. 
      + rewrite xpair_eqE=>/andP [] /eqP H; move:(r_neq_sr fb); rewrite H eqxx=>//.
      + move:Hc =>/negP; auto.
    - move: H Hc1 Hc2 Hc3; case x=>[t0 []] /= H // _ /orP [].
      + move /andP =>[Ht1 _] /norP [Ht2 Ht3].
        move:Ht1=>/eqP Ht1; subst.
        move: (Hinv0 t0 H).
        have: ((t0 + 1, true) \in b == false).
        { rewrite eqbF_neg; apply/negP=>Hc; move: Hfb=>/mapP; apply.
          exists (t0+1, true); auto; simpl. }
        by move/eqP=>-> H'; move:Hfb=>/mapP; apply; eexists; eauto.
      + move=> Ht1 /norP [Ht2 Ht3].
        move: Ht1=>/hasP [[s0 []] Hs1] /andP /= [/eqP Hs2 _] //; subst.
        move: (Hinv0 t0 H); rewrite Hs1=>[] [Hs2 Hs3].
        move: Ht3=>/hasP; apply; exists (t0 -1, false)=>//. }
  move=>->; rewrite /find_free /free; simpl.
  rewrite negb_or [in X in if (if X then _ else _) then _ else _]eq_sym r_neq_sr.
  have: (~~ has (fun x => fb+1 == x.1) b).
  { apply/negP=>/hasP [t Ht1 Ht2]; move:Hnin1=>/mapP; apply; eexists; eauto; apply/eqP; auto. }
  move=>->; simpl=>//.
Qed.
      
Lemma inv1_preserve (f : strategy) (b : board) :
  game f strategy1 true b -> invariant1 b ->
  let: next := [:: ((proj1_sig (f b)), true) & b] in
  let: nexnex := [:: ((proj1_sig (strategy1 next)), false) & next] in
  invariant1 nexnex.
Proof.
  rewrite/invariant1=>H0 Hinv0 i.
  rewrite in_cons=>/orP [/eqP|]; first by inversion 1.
  have Hf:(sval (f b) \notin [seq t.1 | t <-b]) by (case (f b)).
  move Heq: (sval (f b)+1 \in map (fun x=>x.1) b)=>[].
  move:Heq=>/mapP [[x [] H Heq]]; simpl in *; subst.
  - rewrite in_cons=>/orP [/eqP|] Hcond.
    + inversion Hcond; subst; clear Hcond.
      rewrite ![in X in if X then _ else _]in_cons H.
      rewrite (_ : [|| _, _| true] = true); last by repeat (apply/orP; right).
      split.
      * move Heq: (sval (f b) - 1 \in [seq t.1 | t<-b])=>[].
        rewrite !in_cons; apply/or3P; repeat right; apply Or33.
        move: Heq=>/mapP [[fb []] Hfb Hfb']; simpl in *; subst => //.
        have Hnin: ((sval (f b), _ ) \in b = false).
        { move=>s; apply/eqP; rewrite eqbF_neg; apply/negP=>Hc.
          move: Hf=>/mapP=>Hf; apply: Hf; eexists; eauto. }
        by move: (Hinv0 (sval (f b) - 1) Hfb); rewrite -addrA addrN addr0 !Hnin=>//.
        by move:Heq=>/eqP; rewrite eqbF_neg=>Heq;
           move: (strategy_prop1 Hinv0 H Heq)=>/eqP->; rewrite in_cons eqxx=>//.
      * move Heq: ((sval (f b) + 2, true) \in b)=>[];
        move: (Hinv0 _ H); rewrite -addrA Heq.
        rewrite -addrA addrN addr0; move=>[Hc].
        by move:Hf=>/mapP=>Hf; contradiction Hf; eexists; eauto.
        by rewrite addrA=>Hin; rewrite !in_cons Hin; apply/or3P; apply Or33.
    + move: (Hinv0 _ Hcond)=>{Hinv0} Hinv0.
      rewrite ![in X in if X then _ else _]in_cons.
      rewrite (_ : ((i+1, true) == (st1_impl _, false)) = false); last by (
        apply/eqP=>Hc; inversion Hc); simpl.
      have: (~~((i+1, true) == (sval (f b), true))).
      { apply/negP=>/eqP=>Hc.
        move Heq: ((i+1, true) \in b)=>[]; rewrite Heq in Hinv0.
        - move:Hf=>/mapP; apply.
          exists (i+1, true); auto; inversion Hc; simpl; auto.
        - inversion Hc as [Ht]; rewrite -Ht in Hf.
          move:Hf=>/mapP; apply; exists (i+1, false)=>//. }
      rewrite -eqbF_neg=>/eqP=>->; simpl.
      move Heq: ((i+1, true) \in b)=>[]; rewrite Heq in Hinv0.
      * split; rewrite!in_cons; apply/or3P; apply Or33; destruct Hinv0; auto.
      * rewrite !in_cons; apply/or3P; apply Or33; auto.
  - move=>Hin.
    rewrite ![in X in if X then _ else _]in_cons.
    rewrite (_ : ((i+1, true) == (st1_impl _, false)) = false); last by (
        apply/eqP=>Hc; inversion Hc); simpl.
    have: (((i+1, true) == (sval (f b), true)) = false); [|move=>->].
    { apply/negP=>/eqP=>Hc; inversion Hc as [Ht].
      move Heq: ((sval (f b), true) \in b) => [].
      - by move: Hf=>/mapP; apply; exists (sval (f b), true); auto.
      - have Hinb: ((i, true) \in b).
        { move: Hin=>/orP []; auto=>/eqP Heq'.
          inversion Heq' as [Heq'']; subst.
          move:Ht=>/eqP; rewrite eq_sym addrC -subr_eq addrN =>/eqP; inversion 1. }
        move: (Hinv0 i Hinb); rewrite Ht Heq=>Hc'.
        move: Hf=>/mapP; apply; exists (sval (f b), false)=>//. }
    rewrite in_cons in Hin.
    move Heq:((i, true) == (sval (f b), true))=>[]; rewrite Heq in Hin; simpl in *.
    + move: Heq=>/eqP=>Heq; inversion Heq; subst; clear Heq.
      move Heq: ((sval (f b) + 1, true) \in b)=>[].
      + by move: (game_func H0 H Heq (@eqxx _ (sval (f b) + 1)))=>//.
      + by rewrite !in_cons H; apply/or3P; apply Or33=>//.
      move: (Hinv0 _ Hin)=>{Hinv0} Hinv0.
      move Heq': ((i+1, true) \in b)=>[]; rewrite Heq' in Hinv0.
      + by split; rewrite !in_cons; apply/or3P; apply Or33; destruct Hinv0; auto.
      + by rewrite !in_cons; apply/or3P; apply Or33; auto.
  - move=>Hin.
    rewrite ![in X in if X then _ else _]in_cons.
    rewrite (_ : ((i+1, true) == (st1_impl _, false)) = false); last by (
        apply/eqP=>Hc; inversion Hc).
    have Heq': ((sval (f b), _) \in b = false).
    { move=>s; apply/eqP; rewrite eqbF_neg; apply/negP=>Hc. 
      move: Hf=>/mapP; apply; eexists; eauto. }

    move: Hin; rewrite in_cons=>/orP [/eqP|] H.
    { inversion H as [H']; clear H.
      have: ((sval (f b) + 1, true) == (sval (f b), true) = false); [|move=>->].
      { apply/eqP=>H; inversion H as [H'']; move: H''=>/eqP.
        rewrite -subr_eq0 addrC addrA [-_ + _]addrC addrN; cbv; done. }
      have: ((sval (f b) + 1, true) \in b = false); [|move=>->; simpl].
      { apply/eqP; rewrite eqbF_neg; apply /negP=>Hc.
        move:Heq=>/eqP; rewrite eqbF_neg=>/mapP; apply; eexists; eauto. }
      move: Heq=>/eqP; rewrite eqbF_neg=>Heq.
      move: (strategy_prop2 Hinv0 Heq)=>/eqP ->; rewrite in_cons eqxx=>//. }
    { have: ((i + 1, true) == (sval (f b), true) = false); [|move=>->].
      { apply/eqP=>H'; inversion H' as [H''].
        move: (Hinv0 i H); rewrite H''.
        have: ((sval (f b), true) \in b = false); [|move=>->].
        { apply/eqP; rewrite eqbF_neg; apply/negP=> Hc.
          move: Hf=>/negP; apply; apply/mapP; eexists; eauto. }
        rewrite Heq'=>//. }
      move Heq'': ((i+1, true)\in b)=>[]; simpl.
      - move: (Hinv0 _ H); rewrite Heq''=>Hinv0'.
        split; rewrite !in_cons; apply/or3P; apply Or33; tauto.
      - move: (Hinv0 _ H); rewrite Heq''=>Hinv0'.
        rewrite !in_cons; apply/or3P; apply Or33; auto. }
Qed.

Definition A_win (b : board) :=
  exists t, (((t, true) \in b) && ((t+1, true) \in b) && ((t+2, true) \in b)).

Lemma inv_A_win_N (b : board) (f g : strategy) (turn : bool):
  game f g turn b -> invariant1 b -> ~A_win b.
Proof.
  rewrite/invariant1 /A_win=>Hg H [t] /andP [] /andP [] H1 H2 H3.
  move: (H t H1).
  rewrite H2=> [] [H4 H5].
  move: (game_func Hg H3 H5); simpl; rewrite eqxx; auto.
Qed.

Lemma gameA_ind : forall (f g : strategy) (P :  board -> Prop),
  P [::] ->
  (forall b : board, game f g true b -> P b ->
    P ((sval (g ((sval (f b), true) :: b)), false) :: (sval (f b), true) :: b)) ->
  forall (b0 : board), game f g true b0 -> P b0.
Proof.
  intros f g P Pnil Pcons2.
  refine (fix rec (b : board) (gm : game f g true b) {struct gm} :=
    match gm in game _ _ t b' return List.tl b' = List.tl b' -> true = t -> b' = b -> P b' with
      | Game0 => fun _ _ _ => Pnil
      | GameA b gm' => match gm' in game _ _ turn b''
                             return (true = turn -> b = b'' -> _ -> _ -> P _) with
                         | Game0 => fun H H' H'' => _
                         | GameA b g => fun H H' H'' => _
                         | GameD b g => fun H H' H'' => _
                       end erefl
      | GameD b gm' => match gm' in game _ _ turn b''
                             return (false = turn -> b = b'' -> _ -> _ -> P _) with
                         | Game0 => fun H H' H'' H''' => _
                         | GameA b g => fun H H' H'' H''' => _
                         | GameD b g => fun H H' H'' H''' => _
                       end erefl
    end erefl erefl erefl); try now inversion H'.
  apply Pcons2. apply g0. apply rec. apply g0.
Qed.

Lemma game_invariant_A (b : board) (f : strategy) :
  game f strategy1 true b -> invariant1 b.
Proof.
  apply gameA_ind.
  - move=>i; inversion 1.
  - move=>{b} b g1 IH.
    apply inv1_preserve; auto.
Qed.

Lemma game_weaken (f g : strategy) (b : board) :
  let: nb := [:: ((proj1_sig (g b)), false) & b] in
  ~A_win nb -> ~A_win b.
Proof.
  rewrite /A_win=>H [t] /andP [] /andP [] H1 H2 H3.
  apply H; exists t.
  repeat (apply/andP; split); rewrite in_cons; apply/orP; right; auto.
Qed.

Lemma game_invariant (b : board) (f : strategy) (turn : bool) :
  game f strategy1 turn b -> ~A_win b.
Proof.
  case turn=>H.
  - move: (game_invariant_A H); apply (inv_A_win_N H).
  - assert (game f strategy1 true [:: ((proj1_sig (strategy1 b)), false) & b]).
    apply GameD; auto.
    move: (game_invariant_A H0)=> Hinv.
    move: (inv_A_win_N H0 Hinv)=> Awin.
    eapply game_weaken; eauto.
Qed.