Require Import ssreflect ssrfun ssrbool eqtype ssrnat choice seq.
Require Import path fintype finfun bigop ssralg ssrnum poly ssrint.

Inductive formula :=
| Var : nat -> formula
| T   : formula
| F   : formula
| And : formula -> formula -> formula
| Or  : formula -> formula -> formula
| Imp  : formula -> formula -> formula
| Not : formula -> formula.


Definition env := nat -> bool.

Section Denotation.
  Variable e : env.

  Fixpoint denote (f : formula) : bool :=
    match f with
      | T => true
      | F => false
      | Var n => e n
      | And f1 f2 => denote f1 && denote f2
      | Or  f1 f2 => denote f1 || denote f2
      | Imp f1 f2 => ~~ denote f1 || denote f2
      | Not f1 => ~~ denote f1
    end.
End Denotation.

Definition equiv (f1 f2 : formula) : Prop := forall e, denote e f1 = denote e f2.

Example or_comm (f1 f2 : formula) : equiv (Or f1 f2) (Or f2 f1).
Proof.
  move=>env /=; case: (denote env f1); case: (denote env f2) =>//.
Qed.  

Fixpoint fv (f : formula) : seq nat :=
  match f with
    | T => [::]
    | F => [::]
    | Var n => [:: n]
    | And f1 f2 => fv f1 ++ fv f2
    | Or f1 f2 => fv f1 ++ fv f2
    | Imp f1 f2 => fv f1 ++ fv f2
    | Not f => fv f
  end.

Definition override (e : env) n b : env := 
  fun x => if x == n then b else e x.

Fixpoint or_seq (fs : seq formula) : formula := foldr Or T fs.
Fixpoint and_seq (fs : seq formula) : formula := foldr And F fs.

Fixpoint seq_env (order : seq nat) : seq env :=
  match order with
    | [::] => [:: fun _ => false]
    | n :: order =>
      let: ss := seq_env order in
      [seq override e n true | e <- ss] ++ [seq override e n false | e <- ss]
  end.

Definition DNF := seq (seq (bool * nat)).

Definition DNF_of (f : formula) : DNF :=
  let: fs := undup (sort leq (fv f)) in
  let: es := seq_env fs in
  let: trues := [ seq e <- es | denote e f ] in
  [ seq [ seq (e v, v) | v <- fs ] | e <- trues ].

Eval compute in DNF_of (And (Or (Var 0) (Var 1)) (And T (Or (Var 1) (And (Var 2) (Var 0))))).
Eval compute in DNF_of (Or (Var 0) (Not (Var 0))).